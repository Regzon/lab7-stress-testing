#!/usr/bin/env bash

while read -r line; do
  read -ra array <<<"$line"
  echo "Processing ${array[0]}"
  artillery run -q -o reports/report-"${array[0]}".json -v "{\"url\": \"${array[1]}\"}" test.yml
done < urls.txt

echo "Finished all"
